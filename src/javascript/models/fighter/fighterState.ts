import { FighterPosition } from "../enums/fighterPosition";
import { Fighter } from "./fighter";

export interface FighterState {
    isProtected: boolean;
    criticalHitChance: number;
    dodgeChance: number;
    fullHealthVal: number;
    CriticalHitCombinationAvailable: boolean;
    position: FighterPosition;
    fighterInfo: Fighter | null;
}