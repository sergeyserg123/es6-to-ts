import { onCloseFunction } from "../types/onCloseFunction";

export interface ModalElement {
    title: string;
    bodyElement: HTMLElement;
    onClose?: onCloseFunction;
}