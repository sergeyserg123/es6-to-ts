export interface CustomElement {
    tagName?: string;
    className?: string;
    attributes?: any
}