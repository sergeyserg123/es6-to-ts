import { FighterState } from "../fighter/fighterState";

export type onUpdateHealthBar = (defenderState: FighterState, attackerState: FighterState) => void;