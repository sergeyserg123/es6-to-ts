import { CustomElement } from "../models/common/customELement";

export function createElement({ tagName, className, attributes = {} }: CustomElement): HTMLElement {
  const element: HTMLElement = document.createElement(tagName!);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
