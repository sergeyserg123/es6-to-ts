import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/fighter/fighter';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi<Fighter[]>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<Fighter> {
    try {
      const endpoint = `details/fighter/${id}.json`;

      const apiResult: Fighter = await callApi<Fighter>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }    
  }
}

export const fighterService = new FighterService();
