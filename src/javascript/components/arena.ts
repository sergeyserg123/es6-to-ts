import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { Fighter } from '../models/fighter/fighter';

export function renderArena(selectedFighters: Fighter[]): void {
  const root: HTMLElement | null = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root!.innerHTML = '';
  root!.append(arena);

  let [firstFighter, secondFighter] = selectedFighters;

  fight(firstFighter, secondFighter).then(fighter => {
    showWinnerModal(fighter);
  });
}

function createArena(selectedFighters: Fighter[]): HTMLElement {
  const [ff, sf] = selectedFighters;
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(ff, sf);
  const fighters = createFighters(ff, sf);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: Fighter, rightFighter: Fighter): HTMLElement {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: Fighter, position: string): HTMLElement {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` } });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: Fighter, secondFighter: Fighter): HTMLElement {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: Fighter, position: string): HTMLElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
