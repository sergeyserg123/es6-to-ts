import { createElement } from '../helpers/domHelper';
import { Fighter } from '../models/fighter/fighter';

export function createFighterPreview(fighter: Fighter, position: string) {
  const fighterInfo = createFighterInfo(fighter, position);
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  fighterElement.append(fighterInfo, imgElement);

  return fighterElement;
}

export function createFighterImage(fighter: Fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter: Fighter, position: string) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'fighter-preview___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'fighter-preview___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'fighter-preview___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'fighter-preview___health-bar', attributes: { id: `${position}-fighter-indicator` } });
  const healthVal = createElement({ tagName: 'span', className: 'fighter-preview___health-bar-value' });

  healthVal.innerText = `${fighter.health}`;
  fighterName.innerText = `${name} (attack - ${fighter.attack}, defense - ${fighter.defense})`;

  bar.append(healthVal);
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}