import { controls } from '../../constants/controls';
import { FighterPosition } from '../models/enums/fighterPosition';
import { Fighter } from '../models/fighter/fighter';
import { FighterState } from '../models/fighter/fighterState';
import { onUpdateHealthBar } from '../models/types/onUpdateHealthBar';


export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  const firstFighterState = createFighterState(firstFighter, FighterPosition.left);
  const secondFighterState = createFighterState(secondFighter, FighterPosition.right);
  const updateHealthBar = createHealthbarUpdater();

  registerHitsPowerListeners(firstFighterState, secondFighterState, updateHealthBar);
  registerBlocksPowerListeners(firstFighterState, secondFighterState);
  registerCriticalHitCombinationsListeners(firstFighterState, secondFighterState, updateHealthBar);

  return new Promise((resolve) => {
    document.addEventListener('winner', (e: CustomEventInit<{ winner: Fighter }>) => {
      const fighter = e.detail!.winner;
      resolve(fighter);
    });
  });
}


export function getDamage(attacker: Fighter, defender: Fighter): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter: Fighter): number {
  const criticalHitChance = calcChance();
  const hitPower = fighter.attack! * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter: Fighter): number {
  const dodgeChance = calcChance();
  const blockPower = fighter.defense! * dodgeChance;
  return blockPower;
}

function getCriticalDamage(attacker: Fighter): number {
  const damage = 2 * attacker.attack!;
  return damage;
}

function calcChance(): number {
  return Math.random() + 1;
}

function attack(attackerState: FighterState, defenderState: FighterState, updateHealthBar: onUpdateHealthBar): void {
  const damage = getDamage(attackerState.fighterInfo!, defenderState.fighterInfo!);

  if (damage > 0) {
    defenderState.fighterInfo!.health! -= damage;
    updateHealthBar(defenderState, attackerState);
  }
}

function criticalAttack(attackerState: FighterState, defenderState: FighterState, updateHealthBar: onUpdateHealthBar): void {
  const damage = getCriticalDamage(attackerState.fighterInfo!);
  defenderState.fighterInfo!.health! -= damage;
  updateHealthBar(defenderState, attackerState);
  disableCriticalHitCombination(attackerState);
}

function registerHitsPowerListeners(firstFighterState: FighterState, secondFighterState: FighterState, updateHealthBar: onUpdateHealthBar) {
  const onKeyDown = (event: KeyboardEvent) => {
    switch (event.code) {
      case controls.PlayerOneAttack:
        if (!firstFighterState.isProtected && !secondFighterState.isProtected) {
          attack(firstFighterState, secondFighterState, updateHealthBar);
        }
        break;

      case controls.PlayerTwoAttack:
        if (!firstFighterState.isProtected && !secondFighterState.isProtected) {
          attack(secondFighterState, firstFighterState, updateHealthBar);
        }
        break;
    }
  };

  document.addEventListener('keydown', onKeyDown);

  document.addEventListener('deleteEvents', (e) => {
    document.removeEventListener('keydown', onKeyDown);
  });
}

function registerBlocksPowerListeners(firstFighterState: FighterState, secondFighterState: FighterState) {

  const onKeyDown = (event: KeyboardEvent) => {
    switch (event.code) {

      case controls.PlayerOneBlock:
        if (!firstFighterState.isProtected) {
          firstFighterState.isProtected = true;
        }
        break;

      case controls.PlayerTwoBlock:
        if (!secondFighterState.isProtected) {
          secondFighterState.isProtected = true;
        }
        break;
    }
  };

  const onKeyUp = (event: KeyboardEvent) => {
    switch (event.code) {

      case controls.PlayerOneBlock:
        if (firstFighterState.isProtected) {
          firstFighterState.isProtected = false;
        }
        break;

      case controls.PlayerTwoBlock:
        if (secondFighterState.isProtected) {
          secondFighterState.isProtected = false;
        }
        break;
    }
  };

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);

  document.addEventListener('deleteEvents', (e) => {
    document.removeEventListener('keydown', onKeyDown);
    document.removeEventListener('keyup', onKeyUp);
  });
}

function registerCriticalHitCombinationsListeners(firstFighterState: FighterState, secondFighterState: FighterState, updateHealthBar: onUpdateHealthBar) {
  const [KeyQ, KeyW, KeyE] = controls.PlayerOneCriticalHitCombination;
  const [KeyU, KeyI, KeyO] = controls.PlayerTwoCriticalHitCombination;

  const firstPlayerCriticalCombination = new Set();
  const secondPlayerCriticalCombination = new Set();

  const onKeyDown = (event: KeyboardEvent) => {
    switch (event.code) {
      case KeyQ:
        if (firstFighterState.CriticalHitCombinationAvailable)
          firstPlayerCriticalCombination.add(KeyQ);
        break;

      case KeyW:
        if (firstFighterState.CriticalHitCombinationAvailable)
          firstPlayerCriticalCombination.add(KeyW);
        break;

      case KeyE:
        if (firstFighterState.CriticalHitCombinationAvailable)
          firstPlayerCriticalCombination.add(KeyE);
        break;

      case KeyU:
        if (secondFighterState.CriticalHitCombinationAvailable)
          secondPlayerCriticalCombination.add(KeyU);
        break;

      case KeyI:
        if (secondFighterState.CriticalHitCombinationAvailable)
          secondPlayerCriticalCombination.add(KeyI);
        break;

      case KeyO:
        if (secondFighterState.CriticalHitCombinationAvailable)
          secondPlayerCriticalCombination.add(KeyO);
        break;
    }

    if (firstPlayerCriticalCombination.size === controls.PlayerOneCriticalHitCombination.length) {
      criticalAttack(firstFighterState, secondFighterState, updateHealthBar);
      firstPlayerCriticalCombination.clear();
    }

    if (secondPlayerCriticalCombination.size === controls.PlayerTwoCriticalHitCombination.length) {
      criticalAttack(secondFighterState, firstFighterState, updateHealthBar);
      secondPlayerCriticalCombination.clear();
    }
  };

  const onKeyUp = (event: KeyboardEvent) => {
    switch (event.code) {
      case KeyQ:
        firstPlayerCriticalCombination.delete(KeyQ);
        break;

      case KeyW:
        firstPlayerCriticalCombination.delete(KeyW);
        break;

      case KeyE:
        firstPlayerCriticalCombination.delete(KeyE);
        break;

      case KeyU:
        secondPlayerCriticalCombination.delete(KeyU);
        break;

      case KeyI:
        secondPlayerCriticalCombination.delete(KeyI);
        break;

      case KeyO:
        secondPlayerCriticalCombination.delete(KeyO);
        break;
    }
  };

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);

  document.addEventListener('deleteEvents', (e) => {
    document.removeEventListener('keydown', onKeyDown);
    document.removeEventListener('keyup', onKeyUp);
  });
}

function createHealthbarUpdater(): onUpdateHealthBar {
  const leftHealthBar = document.getElementById('left-fighter-indicator')!;
  const rightHealthBar = document.getElementById('right-fighter-indicator')!;

  return function updateHealthBar(defenderState: FighterState, attackerState: FighterState) {
    const { position } = defenderState;
    const healthBar = position == FighterPosition.left ? leftHealthBar : rightHealthBar;
    const fullHealthVal = defenderState.fullHealthVal;
    const restHealth = defenderState.fighterInfo!.health!;

    const result = (100 * restHealth) / fullHealthVal;

    healthBar.style.width = result <= 0 ? '0%' : `${result}%`;
    if (result <= 0) {
      dispatchWinner(attackerState.fighterInfo!);
    }
  }
}

function dispatchWinner(winner: Fighter): void {
  const winnerCustomEvent = new CustomEvent('winner', {
    detail: {
      winner: winner
    }
  });
  const deleteEventsCustomEvent = new CustomEvent('deleteEvents');
  document.dispatchEvent(winnerCustomEvent);
  document.dispatchEvent(deleteEventsCustomEvent);
}

function disableCriticalHitCombination(fighterState: FighterState): void {
  const criticalHitTimeout = 10000;

  fighterState.CriticalHitCombinationAvailable = false;
  setTimeout(() => {
    fighterState.CriticalHitCombinationAvailable = true;
  }, criticalHitTimeout);
}

function createFighterState(fighter: Fighter, position: FighterPosition): FighterState {
  const fighterState: FighterState = {
    isProtected: false,
    criticalHitChance: 0,
    dodgeChance: 0,
    fullHealthVal: fighter.health!,
    CriticalHitCombinationAvailable: true,
    fighterInfo: fighter,
    position: position
  };
  return fighterState;
}
