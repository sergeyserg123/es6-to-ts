import { showModal } from "./modal";
import { createFighterImage } from '../fighterPreview';
import { Fighter } from "../../models/fighter/fighter";

export function showWinnerModal(fighter: Fighter): void {
  const title = `Fighter ${fighter.name} win!`;
  const imgElement = createFighterImage(fighter);

  showModal({ title: title, bodyElement: imgElement });
}
