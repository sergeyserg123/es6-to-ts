declare class App {
    static rootElement: HTMLElement;
    static loadingElement: HTMLElement;
    startApp: Promise<void>;
}